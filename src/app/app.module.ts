import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { Routes, RouterModule } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { AlertModule } from 'ngx-bootstrap/alert';
import { BootstrapComponent } from './bootstrap/bootstrap.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { LoginComponent } from './login/login.component';
import { ApiTokenService} from "./services/api-token.service";
import { AuthService } from "./services/auth.service";
import { DefaultRequestOptionsService } from "./services/default-request-options.service";
import { DefaultRequestHttpHeadersService } from "./services/default-request-http-headers.service";
import { AuthGuardRouterService } from "./services/auth-guard-router.service";
import { LocalStorageService } from "./services/local-storage.service";
import {HttpClientTestService} from "./services/HttpClientTestService";
import { NavbarComponent } from './navbar/navbar.component';
import { PageRestrictComponent } from './page-restrict/page-restrict.component';
import { LogoutComponent } from './logout/logout.component';


const appRouter:Routes = [
    {path: 'login', component:LoginComponent},
    {path: '', redirectTo:'/login', pathMatch:'full' },
    {path: 'bootstrap', component:BootstrapComponent},
    {path: 'restricted-page', component:PageRestrictComponent, canActivate:[AuthGuardRouterService]},
    {path: 'logout', component:LogoutComponent},
];
@NgModule({
  declarations: [
    AppComponent,
    BootstrapComponent,
    LoginComponent,
    NavbarComponent,
    PageRestrictComponent,
    LogoutComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRouter),
    HttpClientModule,
    BsDropdownModule.forRoot(),
    AlertModule.forRoot(),
    AngularFontAwesomeModule,
  ],
  providers: [
      ApiTokenService,
      AuthService,
      DefaultRequestOptionsService,
      DefaultRequestHttpHeadersService,
      AuthGuardRouterService,
      LocalStorageService,
      HttpClientTestService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
