import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { ApiTokenService } from "../services/api-token.service";
import { Router } from "@angular/router";
import {AuthService} from "../services/auth.service";

declare let $;
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    errorMessage = '';
    e = false;

    // "username": "testuser",
    // "password": "testpass",

    keys = {
        grant_type: "password",
        username: "testuser",
        password: "testpass",
        client_id: "testclient2"
    };

    constructor(private http: HttpClient, private apiToken: ApiTokenService, private router: Router, private authServer: AuthService) {}


    ngOnInit() {}

    checkPermissions(){
        this.http.post("http://127.0.0.1:8080/oauth", this.keys)
            .subscribe( data => {
                console.log(data),
                    this.apiToken.token = data['access_token'],
                    this.authServer.login();
                    this.router.navigate(['/restricted-page']);
                },
                (err: HttpErrorResponse) => {
                    if (err.error instanceof Error) {
                        this.errorMessage = "Ocorreu um erro no lado do cliente. " + err.status
                        this.addRow();
                    } else {
                        if (err.status === 401){
                            this.errorMessage = `Dados incorretos! Tente novamente.  Erro status: ` + err.status;
                            this.addRow();
                            return;
                        }
                        this.errorMessage = "Ocorreu um erro no lado do servidor. Erro status: " + err.status
                        this.addRow();
                    }
                }

            )
    }

    addRow() {
        this.e = true;
        $("#divError").delay('fast').fadeIn();
        $("#divError").delay(4000).fadeOut(function () {
            this.e = false;
        });
    }

}

