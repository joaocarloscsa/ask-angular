import { Injectable } from '@angular/core';
import {RequestOptions, RequestOptionsArgs, Headers} from "@angular/http";
import { HttpClient, HttpHeaders  } from "@angular/common/http";
import {ApiTokenService} from "./api-token.service";

@Injectable()
export class DefaultRequestOptionsService extends RequestOptions{

  constructor(private apiToken: ApiTokenService) {

    super();
  }

    merge(options?: RequestOptionsArgs): RequestOptions {
      let headers = (options.headers) ? options.headers : new Headers();
      headers.set('Authorization', `Bearer ${this.apiToken.token}`);
      headers.set('Content-Type', 'application/json');
      options.headers = headers;
      return super.merge(options);
    }


}
