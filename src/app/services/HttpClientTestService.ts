import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import {ApiTokenService} from "./api-token.service";
import {DefaultRequestOptionsService} from "./default-request-options.service";
import {DefaultRequestHttpHeadersService} from "./default-request-http-headers.service";

@Injectable()
export class HttpClientTestService {

    constructor(private http: HttpClient,
                private apiToken: ApiTokenService,
                private defaultRequestOptions: DefaultRequestOptionsService,
                private defaultRequestHttpHeaders: DefaultRequestHttpHeadersService
    ) {}

    query(): Observable<Object> {
        // return this.http.get("http://127.0.0.1:8080/ping", {headers: new HttpHeaders().set('Authorization', `Bearer ${this.apiToken.token}`)});
         return this.http.get("http://127.0.0.1:8080/ping",  {headers: this.defaultRequestHttpHeaders.httpHeaders} );
        // return this.http.get("http://127.0.0.1:8080/ping", , {headers: this.defaultRequestOptions.merge(new Headers())});
    }

}
