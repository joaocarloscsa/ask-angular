import { Injectable } from '@angular/core';
import {LocalStorageService} from "./local-storage.service";
const TOKEN_KEY = 'access_token';
@Injectable()
export class ApiTokenService {

  constructor(private localStorage: LocalStorageService) { }


  set token(value){
      (value !== null) ? this.localStorage.set(TOKEN_KEY, value) : this.localStorage.remove(TOKEN_KEY);
  }

  get token(){
    return this.localStorage.get(TOKEN_KEY);
  }
}
