import { Injectable } from '@angular/core';
import { HttpHeaders  } from "@angular/common/http";
import {ApiTokenService} from "./api-token.service";

@Injectable()
export class DefaultRequestHttpHeadersService {

  constructor(private apiToken: ApiTokenService) { }

    get httpHeaders(): HttpHeaders {
      //  let headers = new HttpHeaders();
      //  headers.set('Authorization', `Bearer ${this.apiToken.token}`);
      //  headers.set('Content-Type', 'application/json');
       let headers = new HttpHeaders({'Authorization': `Bearer ${this.apiToken.token}`, 'Content-Type': 'application/json'});
       //   headers.append('Content-Type', 'application/json');
      return headers;
    }
}
