import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageService {

    constructor() {
    }

    set(key, value) {
        window.localStorage.setItem(key, value);
        return this;
    }

    get(key, defaultValue = null) {

        // return window.localStorage[key] || defaultValue;
        return window.localStorage.getItem(key);
    }

    setObject(key, value: Object) {
        window.localStorage.setItem(key, JSON.stringify(value));
        return this;
    }

    getObject(key) {
        return JSON.parse(window.localStorage.getItem(key))
    }

    remove(key){
        window.localStorage.removeItem(key);
    }
}