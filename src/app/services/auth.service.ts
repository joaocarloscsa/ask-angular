import { Injectable } from '@angular/core';
import {ApiTokenService} from "./api-token.service";

@Injectable()
export class AuthService {
public check:Boolean = false;
  constructor(private apiToken: ApiTokenService) {
    this.check = this.apiToken.token ? true : false;
  }

  logout(){
      this.apiToken.token = null;
      this.check = false;
  }
    login(){
        this.check = true;
    }
}
