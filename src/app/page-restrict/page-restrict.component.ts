import {Component, OnInit} from '@angular/core';
import {HttpErrorResponse} from "@angular/common/http";
import {HttpClientTestService} from "../services/HttpClientTestService";

declare let $;

@Component({
    selector: 'app-page-restrict',
    templateUrl: './page-restrict.component.html',
    styleUrls: ['./page-restrict.component.css']
})
export class PageRestrictComponent implements OnInit {
    successMessage = '';
    errorMessage = '';
    e = false;

    constructor(private httpClientTest: HttpClientTestService) {
    }

    ngOnInit() {
    }

    submitRequestTest() {
        this.httpClientTest.query()
            .subscribe(data => {
                    this.successMessage = JSON.stringify(data)
                },
                (err: HttpErrorResponse) => {
                    if (err.error instanceof Error) {
                        this.errorMessage = "Ocorreu um erro no lado do cliente. " + err.status
                        this.addRow();
                    } else {
                        if (err.status === 401) {
                            this.errorMessage = `Dados incorretos! Tente novamente.  Erro status: ` + err.status;
                            this.addRow();
                            return;
                        }
                        this.errorMessage = "Ocorreu um erro no lado do servidor. Erro status: " + err.status
                        this.addRow();
                    }
                }
            )
    }

    addRow() {
        this.e = true;
        $("#divError").delay('fast').fadeIn();
        $("#divError").delay(4000).fadeOut(function () {
            this.e = false;
        });
    }
}


